# Limitless Web Client

The Limitless Web Client application in this repo is designed to allow administrators of Limitless to perform a variety of actions:

- **Admin**: view all submission with pagination to check the status and participant details, change the Limitless phase, and initiate judging video creation pipeline

- **Moderation**: view the transcoded and transcribed submissions with tags, language, and raw transcripts for moderation

  - _Approve_: approve the submission
  - _Reject_: reject the submission by providing a rejection reason
  - _Share Link_: if not sure about the moderation result, share the single moderation link to National Societies for moderation

- **Transcription**: all submissions will go through AWS Transcribe / Google Speech-to-Text / YouTube Automatic Caption to get machine-generated subtitles. Admin users will be able to go to YouTube Editor to do post-editing jobs to modify and publish the caption in the submission source language

- **Translation**：post-editing for machine-generated translations for both static communication content, website text, and submission videos (via YouTube Editor)

- **Verification**: verify the transcription & translation jobs by adjusting the score multiplier or reject the job to push to task back to the queue

## Data Structure

The following section provides an overview of the Firestore data structure relevant to Limitless Web Client:

**User Objects**

```
- users
    - {firebase_uid}
        - linkedin_id # optional for email login users
        - photo_url
        - user_name
        - roles
        - score
        - created_at
        - updated_at
```

**Submission Objects**

```
- submissions
    - {submission_id}
        - lang # language for the submission
        - commslanguage # preferred communication language
        - navigatorLocale # browser locale
        - phase # submission phase
        - region # associated National Society
        - status # 'draft' | 'submitted' | 'readyformoderation' | 'moderated' | 'edited'
        - submitted_by # reference to the user who submitted
        - tags # an array of tags for the submission
        - reject_reason # if rejected by moderator, the reason for rejection
        - error # if error, record the reason for error
        - media # an array of media objects references
        - created_at
        - updated_at
```

**Media Objects**

```
- media
    - {media_id}
        - src # s3 key if the media object is a video, image, or caption
        - modality # 'text' for text-based content
        - type # 'image' | 'hd_video' | 'square_video' | 'caption' | 'raw' | 'raw_transcript' | 'social' | 'static_article' | 'static_image' | 'static_locale' | 'static_video' | 'hd_video_transcoded'
        - status # 'autotranslated' | 'transcoded' | 'edited' | 'readyformanualtranslate' | 'finalised'
        - langs # an array of lang object references
        - submission # `submissions/${submission_id}`
        - srcLang # (VIDEO) submission source language
        - youtube # (VIDEO)youtube id if the media object is 'hd_video' or 'caption'
        - uploadedAt # (VIDEO)
        - duration # (VIDEO) duration of the video clip
        - clips # (VIDEO) timestamps for each video clio if the media object is 'hd_video'
        - transcribedBy # (VIDEO) uid for transcription
        - transcribedAt # (VIDEO)
        - content # (STATIC) content reference for the static comms object
        - description # (STATIC) description for the static comms object
        - created_at
        - updated_at
```

**Lang Objects**

```
- langs:
    - {lang_id}
        - submission # `media/${media_id}`
        - original # set if thie is the source text
        - description # (STATIC) description for the static comms object
        - status # 'autotranslated' | 'failedautotranslated' | 'readyformanualtranslate' | 'finalised'
        - srcText
        - srcLang
        - targetText
        - targetLang
        - targetLangArr # when the text is longer than 5,000 bytes, need to split into chunks for AWS Translate, use the `targetLangArr` for concatenating the translations
        - translatedBy # uid for translation
        - translatedAt
        - verifiedBy # uid for verification
        - verifiedAt
        - score # score for the verification task
        - multiplier # multiplier for calculating the translation task score
        - created_at
        - updated_at
```

**Config Objects**

```
- config
    - stats # basic statistics for each admin task
        - readyformoderation
        - readyformanualsubtitle
        - readyformanualtranslate
        - readyforverify
    - userRoles
        - users
            - {user_email}
                - roles # pre-defined roles for all admin users
    - leaderboard
        - langs # an array of languages with the completion percentage
            - completed
            - lang
        - perday # average daily score among all participants
        - perperson # average score among all participants
        - toplist # top scoring participants with their names and score
            - name
            - score
    - meta
        - score_map
            - type # score allocated for each object type
        - tag_options # an array of tags used in Limitless (UN's Humanitarian Icons)
        - target_language # target languages supported by Limitless
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# Generating Language files:

`npx vue-i18n-extract use-config`
