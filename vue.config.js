// const MomentLocalesPlugin = require("moment-locales-webpack-plugin");
module.exports = {
  pluginOptions: {
    quasar: {
      importStrategy: "kebab",
      rtlSupport: true,
    },
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: true,
    },
  },
  transpileDependencies: ["quasar"],
  // configureWebpack: {
  //   plugins: [
  //     // To strip all locales except “en”
  //     new MomentLocalesPlugin(),

  //     // Or: To strip all locales except “en”, “es-us” and “ru”
  //     // (“en” is built into Moment and can’t be removed)
  //     new MomentLocalesPlugin({
  //       localesToKeep: ["fr", "es", "ar"],
  //     }),
  //   ],
  // },
};
